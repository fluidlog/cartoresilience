// ============================================================================
//
// thisGraph.openNode(d3Node, dNode)
//
// ============================================================================


FluidGraph.prototype.openNode = function(d3Node, dNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("openNode start");

  //Close precedent opened node
  if (thisGraph.state.openedNode)
    thisGraph.closeNode.call(thisGraph, "opened");

  if (thisGraph.state.editedNode)
    thisGraph.closeNode.call(thisGraph, "edited");

  var el = d3Node;
  var p_el = d3.select(d3Node.node().parentNode); //p_el = g#node

  if (thisGraph.config.clicOnNodeAction == "flod")
  {
    el.select("#fo_content_closed_node_label").remove();
    el.select("#fo_content_closed_node_events").remove();
  }

  var neighbours = thisGraph.getNeighbourNodesAndLinks(dNode);
  var nbNeighbours = neighbours.nodes.length-1;
  var heightNeighbours = nbNeighbours
                          *(thisGraph.customNodes.heightOpenedNeighbour+5)
                          //+nbNeighbours*3; //stroke of each neighbour
                          //+thisGraph.customNodes.heightGhostNeighbour*7 + 10;
  if (heightNeighbours > thisGraph.customNodes.heightOpenedNeighboursMax)
    heightNeighbours = thisGraph.customNodes.heightOpenedNeighboursMax;

  var totalHeight = thisGraph.customNodes.heightOpenedOption
                    + thisGraph.customNodes.heightOpenedLabel
                    + heightNeighbours

  var xNodeCircle = thisGraph.customNodes.widthOpened / 2
  var yNodeCircle = totalHeight/2;
  var cyCircleIndex = totalHeight/2-5;
  var dyTextIndex = (totalHeight/2)-10;
  var cyCircleType = (totalHeight/2)-10;
  var yTypeImage = (totalHeight/2)-22;
  var ylabel = totalHeight/2;

  var totalWidth;

  el
    .select("#circle_index")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("cy", -cyCircleIndex)

  el
    .select("#text_index")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("dy", -dyTextIndex)

  el
    .select("#circle_type")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("cy", cyCircleType)

  el
    .select("#fo_type_image")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("y", yTypeImage)

  // el
  //   .select("#fo_content_closed_node_label")
  //   .transition()
  //   .duration(thisGraph.customNodes.transitionDurationOpen)
  //   .delay(thisGraph.customNodes.transitionDelay)
  //   .ease(thisGraph.customNodes.transitionEasing)
  //   .attr("y", -ylabel)

  if (thisGraph.config.clicOnNodeAction == "options")
  {
    xNodeCircle = xNodeCircle/2;
    yNodeCircle = yNodeCircle/2
    totalWidth = thisGraph.customNodes.widthOpened/2;
    totalHeight = totalHeight/2;
  }
  else {
    totalWidth = thisGraph.customNodes.widthOpened;
  }


  el
    .select("#nodecircle")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("x", -xNodeCircle)
    .attr("y", -yNodeCircle)
    .attr("width", totalWidth)
    .attr("height", totalHeight)
    .attr("rx", thisGraph.customNodes.curvesCornersOpenedNode)
    .attr("ry", thisGraph.customNodes.curvesCornersOpenedNode)
    .on("end", function(d) {
        thisGraph.displayContentOpenedNode.call(thisGraph, d3.select(this), d, neighbours, heightNeighbours)
        if (thisGraph.config.displayFlodPanel)
          thisGraph.displayContentInFlodPanelId.call(thisGraph, d, neighbours)
    })

  thisGraph.state.openedNode = d3Node.node();

  if (thisGraph.config.debug) console.log("openNode end");
}

FluidGraph.prototype.displayContentInFlodPanelId = function(dNode, neighbours) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayContentInFlodPanelId start");

  d3.select("#flod_panel_content").remove();
  let flodPanelDiv = d3.select('#flodPanelId')

  let flodPanelDivStyle =
      "background:" + thisGraph.customNodes.colorType[dNode["@type"]] + ";"
      +"stroke:"+ thisGraph.customNodes.strokeColorType[dNode["@type"]] + ";"
      +"stroke-width:"+ thisGraph.customNodes.strokeWidth + ";";

  flodPanelDiv.attr("style",flodPanelDivStyle)

  let flodPanelContentDiv = flodPanelDiv.append('div')
  let flodPanelContentDivStyle =
      "background:" + thisGraph.customNodes.colorType[dNode["@type"]] + ";"
      +"stroke:"+ thisGraph.customNodes.strokeColorType[dNode["@type"]] + ";"
      +"stroke-width:"+ thisGraph.customNodes.strokeWidth + ";";

  flodPanelContentDiv.attr("class", "flod_panel_content")
  .attr("id", "flod_panel_content")
  .attr("style",flodPanelContentDivStyle)

  let flodPanelContentDivLabel = flodPanelContentDiv.append('div')
  .attr("class", "flod_panel_content_label")
  .attr("id", "flod_panel_content_label")
  .text(dNode.label)

  let flodPanelCommentDivStyle =
      "background:" + thisGraph.customNodes.neighbourColorType[dNode["@type"]] + ";"

  flodPanelContentDiv.append('div')
  .attr("class", "flod_panel_content_comment")
  .attr("id", "flod_panel_content_comment")
  .text(dNode.comment)
  .attr("style",flodPanelCommentDivStyle)

  let flodPanelDescriptionDivStyle =
      "background:" + thisGraph.customNodes.neighbourColorType[dNode["@type"]] + ";"

  flodPanelContentDiv.append('div')
  .attr("class", "flod_panel_content_description")
  .attr("id", "flod_panel_content_description")
  .attr("style",flodPanelDescriptionDivStyle)
  .text(dNode.description)

  flodPanelContentDiv.append('div')
  .attr("class", "flod_panel_content_homepage")
  .attr("id", "flod_panel_content_homepage")
  .attr("style",flodPanelDescriptionDivStyle)
  .append("a")
  .attr("href", dNode.hypertext)
  .attr("target", "_blank")
  .text(dNode.hypertext)

  if (thisGraph.config.debug) console.log("displayContentInFlodPanelId end");
}

FluidGraph.prototype.displayContentOpenedNode = function(d3Node, dNode, neighbours, heightNeighbours) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayContentOpenedNode start");

  var el = d3Node.node();
  var p_el = d3.select(d3Node.node().parentNode); //p_el = g#node
  var totalHeight;

  if (thisGraph.config.clicOnNodeAction == "flod")
  {
    totalHeight = thisGraph.customNodes.heightOpenedOption // (-top > + bottom)
                    + thisGraph.customNodes.heightOpenedLabel
                    + heightNeighbours;
  }
  else if (thisGraph.config.clicOnNodeAction == "options")
  {
    totalHeight = thisGraph.customNodes.heightOpenedOption // (-top > + bottom)
                  + thisGraph.customNodes.heightOpenedLabel
  }

  // console.log("totalHeight : ",totalHeight)
  // console.log("y : ",-(totalHeight/2)-thisGraph.customNodes.heightOpenedOption)
  // console.log("heightNeighbours : ",heightNeighbours)

  // var totalHeightMoreOption = totalHeight+thisGraph.customNodes.heightOpenedOption;

  var xNode = -thisGraph.customNodes.widthOpened/2;
  var yNode = -(totalHeight/2)-thisGraph.customNodes.heightOpenedOption

  var fo_content_opened_node = p_el
        .append("foreignObject")
        .attr("id","fo_content_opened_node")
        .attr("x", xNode)
        .attr("y", yNode)
        .attr("width", thisGraph.customNodes.widthOpened)
        .attr("height", totalHeight)

  var fo_xhtml_content_open_node = fo_content_opened_node
        .append('xhtml:div')
        .attr("class", "fo_xhtml_content_open_node")
        .attr("id", "fo_xhtml_content_open_node")
        //Warning : using css doesn't work !?
        .attr("style","cursor:"+thisGraph.customNodes.cursor+";"
                      +"position:static;"
                      +"width:"+thisGraph.customNodes.widthOpened+"px;"
                      // +"height:"+totalHeightMoreOption+"px;"
                    )
        .on("mousedown",null)
        .on("mouseup",null)
        // .on("mouseover",function(d){
        //   console.log("mouseover on fo_xhtml_content_open_node")
        //   return null;
        // })
        // .on("mouseout",function(d){
        //   console.log("mouseout on fo_xhtml_content_open_node")
        //   thisGraph.nodeOnMouseOut.call(thisGraph, d3.select(this.parentNode.parentNode), d);
        // })
        // .on("dblclick",function(){
        //   console.log("dblclick on fo_xhtml_content_open_node")
        //   d3.event.stopPropagation();
        // })
        // .on("click", function(){
        //   console.log("click on fo_xhtml_content_open_node")
        //   d3.event.stopPropagation();
        // })

    /*
     * Options/action : Hypertext, delete...
     * */
    var xNodeOption = xNode;
    var yNodeOption = yNode-20;
    thisGraph.displayOptionMenu(fo_xhtml_content_open_node,
                                xNodeOption,
                                yNodeOption,
                                dNode,
                                "opened",
                                "node",
                                "large");

    /*
     *
     * Label
     *
     * */
     if (thisGraph.config.clicOnNodeAction == "flod")
     {
    var fo_xhtml_content_open_node_label = fo_xhtml_content_open_node
          .append('div')
          .attr("class", "fo_xhtml_content_open_node_label")
          .attr("id", "fo_xhtml_content_open_node_label")
          //Warning : using css doesn't work !?
          .attr("style", "width:"+thisGraph.customNodes.widthOpened+"px;"
                        // +"height:"+thisGraph.customNodes.heightOpenedLabel+"px;"
                        +"cursor:"+thisGraph.customNodes.cursorOpen+";"
                        +"position:static;")
          .text(function(d){
            if (d.label.length > thisGraph.customNodes.maxCharactersInOpenNodeLabel)
              label = d.label.substring(0,thisGraph.customNodes.maxCharactersInOpenNodeLabel)+" ...";
            else {
              label = d.label;
            }
            return label;
          })
        }

    /*
     *
     * neighbours
     *
     * */

  if (thisGraph.config.clicOnNodeAction == "flod")
  {
    var heightNeighboursBox = heightNeighbours+10;
    var neighbours_box = fo_xhtml_content_open_node
       .append("div")
       .attr("class", "neighbours_box")
       .attr("id", "neighbours_box")
       .attr("style","background-color:rgba(" + thisGraph.customNodes.neighbourColorTypeRgba[dNode["@type"]]
                                   + "," + thisGraph.customNodes.strokeOpacity + ");"
                    +"height:"+heightNeighboursBox+"px;"
                     +"width:"+thisGraph.customNodes.widthOpenedNeighboursBox+"px;"
                     +"overflow: auto;"
                    //  + "border: 2px solid rgba("
                    //  + thisGraph.customNodes.neighbourColorTypeRgba[dNode["@type"]] + ","
                    //  + thisGraph.customNodesText.strokeOpacity + ");"
                     + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                     + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                     + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                     + "display: flex;"
                     + "flex-direction:colomn;"
                     + "justify-content:center;"
                     + "align-content:center;"
                     + "align-items:center;"
                     + "cursor:auto;"
        )

                                            // +"pointer-events: none;")
                              // .on("mousedown",function(d){
                              //   console.log("mousedown on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("mouseup",function(d){
                              //   console.log("mouseup on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("mouseover",function(d){
                              //   console.log("mouseover on neighbours_box");
                              //   return null;
                              // })
                              // .on("mouseout",function(d){
                              //   console.log("mouseout on neighbours_box");
                              //   return null;
                              // })
                              // .on("dragstart", function(d){
                              //   console.log("dragstart on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("drag", function(d){
                              //   console.log("drag on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("dragend", function(d){
                              //   console.log("dragend on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("dblclick",function(){
                              //   console.log("dblclick on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("click", function(){
                              //   console.log("click on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("zoom", function(){
                              //   console.log("zoom on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })
                              // .on("dblclick.zoom", function(){
                              //   console.log("dblclick.zoom on neighbours_box");
                              //   d3.event.stopPropagation();
                              // })

      var heightNodeNeighbours = heightNeighbours+5;
     var node_neighbours = neighbours_box
                               .append("div")
                               .attr("class", "node_neighbours")
                               .attr("id", "node_neighbours")
                               .attr("style","height:"+heightNodeNeighbours+"px;"
                                              +"width:"+thisGraph.customNodes.widthOpenedNeighbours+"px;"
                                              )

    neighbours.nodes.sort(function(a, b){
      var aIndex = thisGraph.customNodes.listType.indexOf(a["@type"]);
      var bIndex = thisGraph.customNodes.listType.indexOf(b["@type"]);

      if (aIndex < bIndex)
        return -1;
      if (aIndex > bIndex)
        return 1;
      return 0;
    })

    var previousType = "";
    var previousNode;
    var neighbour_type_box;
    var labelType;
    var type;

    var nbNeighbours = neighbours.nodes.length-1 //without node itself

    thisGraph.customNodes.listType.forEach(function (typeInList, indexList){
      labelType = typeInList.split(":").pop();

      var existType;

      if (thisGraph.config.addNewNeighbourInFlod == true)
        existType = true;
      else
        existType = false;

      neighbours.nodes.forEach(function(node){
        if (node["@type"] == typeInList && node.index != dNode.index)
        {
          existType = true;
        }
      })

      if (existType == true)
      {
          neighbour_type_box = node_neighbours
          .append("div")
          .attr("class", "neighbour_type_box")
          .attr("id", "neighbour_type_box_"+labelType)
          .attr("style", "background-color:rgba(" + thisGraph.customNodes.colorTypeRgba[typeInList]
                                      + "," + thisGraph.customNodes.strokeOpacity + ");"
                                      // + "border: 2px solid rgba("
                                      // + thisGraph.customNodes.strokeNeighbourColorTypeRgba[typeInList] + ","
                                      // + thisGraph.customNodes.strokeOpacity + ");"
                                      + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                      + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                      + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                      + "cursor:pointer;"
                                      + "padding:3px;"
                                      + "text-align: center;"
                                    )
        }

        if (thisGraph.config.addNewNeighbourInFlod == true)
          // Add Ghost neighbour in box for each type
          thisGraph.addGhostNeighbours(neighbour_type_box, dNode, typeInList);

        neighbours.nodes.forEach(function(node){
          if (node["@type"] == typeInList && node.index != dNode.index)
          {
            // Liste of neighbours for the type
            thisGraph.addNeighbourInBox(neighbour_type_box, node, xNode, yNode)
          }
        })

    })
  }

  thisGraph.MakeNodeLastInSvgSequence(dNode);

  if (thisGraph.config.debug) console.log("displayContentOpenedNode end");
}

FluidGraph.prototype.addNeighbourInBox = function(neighbour_type_box, dNode, xNode, yNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("addNeighbourInBox start");

  var neighboursLabel;
  if (dNode.label.length > thisGraph.customNodes.maxCharactersInNeighbours)
    neighboursLabel = dNode.label.substring(0,thisGraph.customNodes.maxCharactersInNeighbours)+" ...";
  else {
    neighboursLabel = dNode.label;
  }

  var labelType = dNode["@type"].split(":").pop();

  // var toto = $("#neighbour_type_box_"+labelType);
  // var xNode = $("#neighbour_type_box_"+labelType).position().left;
  // var yNode = $("#neighbour_type_box_"+labelType).position().top;

  var node_neighbour = neighbour_type_box
    .insert("div", "#ghost_neighbour_"+labelType)
    .attr("class", "node_neighbour")
    .attr("id", "node_neighbour_"+labelType+"_"+dNode.index)
    .attr("style", function(d) {
      return "background-color:rgba(" + thisGraph.customNodes.neighbourColorTypeRgba[dNode["@type"]]
                                  + "," + thisGraph.customNodesText.strokeOpacity + ");"
                                  // + "border: 2px solid rgba("
                                  // + thisGraph.customNodes.strokeNeighbourColorTypeRgba[dNode["@type"]] + ","
                                  // + thisGraph.customNodesText.strokeOpacity + ");"
                                  + "cursor:pointer;margin:1px;"
                                  + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "display: flex;"
                                  + "flex-direction:row;"
                                  + "justify-content:space-between;"
                                  + "align-content:center;"
                                  + "align-items:center;"
    })

  var node_neighbour_text = node_neighbour
    .append("div")
    .attr("class", "node_neighbour_text")
    .attr("id", "node_neighbour_text_"+labelType+"_"+dNode.index)
    .style("text-align", "center")
    .style("width", "100%")
    .text(neighboursLabel)
    .on("click",function(){
      var d3Node = thisGraph.getD3NodeFromIdentifier(dNode["@id"]);
      if (d3Node)
      {
        thisGraph.centerNode(dNode);
        thisGraph.openNode(d3Node, dNode);
      }
    })

  var xNodeOption = xNode;
  var yNodeOption = yNode;
  thisGraph.displayOptionMenu(node_neighbour,
                              xNodeOption,
                              yNodeOption,
                              dNode,
                              "opened",
                              "neighbour",
                              "small");

  if (thisGraph.config.debug) console.log("addNeighbourInBox end");
}

FluidGraph.prototype.displayOptionMenu = function(parentDiv,
                                                  x,
                                                  y,
                                                  dNode,
                                                  typeOpenEdit,
                                                  typeObject,
                                                  size) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayOptionMenu start");

  // d3.select("#open_neighbour_option").remove();

  optionDiv = parentDiv.append("xhtml:div")
                        .attr("class", "open_"+typeObject+"_option")
                        .attr("id", "open_"+typeObject+"_option")
                        .attr("style","background-color:rgba(" + thisGraph.customNodes.colorTypeRgba[dNode["@type"]]
                                                    + "," + thisGraph.customNodesText.strokeOpacity + ");"
                                                    // + "border: 1px solid rgba("
                                                    // + thisGraph.customNodes.strokeNeighbourColorTypeRgba[dNode["@type"]] + ","
                                                    // + thisGraph.customNodesText.strokeOpacity + ");"
                                                    + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                                    + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                                    + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                                    + "cursor:pointer;"
                                                    + "padding:2px;"
                                                    + "display: flex;"
                                                    + "flex-direction:row;"
                                                    + "justify-content:flex-end;"
                                                    + "align-content:center;"
                                                    + "align-items:center;"
                                                  )

  if (thisGraph.config.customOptions.edit)
  {
    if (typeOpenEdit == "opened")
    {
      var fo_xhtml_edit_image = optionDiv
      .append('div')
      .append('i')
      .attr("id", "fo_i_edit_image")
      .attr("class", "ui "+ size +" edit icon")
      .attr("style", "display:inline;"+"cursor:pointer;")
      .on("click",function(){
        var d3Node = thisGraph.getD3NodeFromIdentifier(dNode["@id"]);
        thisGraph.editNode.call(thisGraph, d3Node, dNode);
      })
      .append("title").text("Edit the node")
    }
    else { // edited
      var fo_xhtml_edit_image = optionDiv
        .append('div')
         .append('i')
          .attr("id", "fo_i_save_image")
          .attr("class", "ui "+ size +" save icon")
        .attr("style", "display:inline;"+"cursor:pointer;")
        .on("click",function(){
          thisGraph.closeNode.call(thisGraph, "edited");
        })
        .append("title").text("Open the node")
    }
  }

  if (thisGraph.config.customOptions.center)
  {
    var fo_xhtml_center_image = optionDiv
      .append('div')
       .append('i')
        .attr("id", "fo_i_center_image")
        .attr("class", "ui "+ size +" crosshairs icon")
      .attr("style", "display:inline;"+"cursor:pointer;")
      .on("click",function(){
        thisGraph.centerNode(dNode);
      })
      .append("title").text("Delete the node")
  }

  if (thisGraph.config.customOptions.focusContextOn)
  {
    var fo_xhtml_focus_image = optionDiv
        .append('div')
         .append('i')
          .attr("id", "fo_i_focus_image")
          .attr("class", "ui "+ size +" dot circle outline icon")
        .attr("style", "display:inline;"+"cursor:pointer;")
        .on("click",function(){
          thisGraph.focusContextNodeOn(dNode);
        })
        .append("title").text("Focus on node")
  }

  if (thisGraph.config.customOptions.focusContextOff)
  {
    if (thisGraph.state.focusMode)
    {
      var fo_xhtml_focus_off_image = optionDiv
          .append('div')
           .append('i')
            .attr("id", "fo_i_focus_off_image")
            .attr("class", "ui "+ size +" react icon")
          .attr("style", "display:inline;"+"cursor:pointer;")
          .on("click",function(){
            thisGraph.focusContextNodeOff(dNode);
          })
          .append("title").text("Delete the node")
    }
  }

  if (thisGraph.config.customOptions.hypertext)
  {
    if (dNode.hypertext)
    {
      var fo_xhtml_external_link_image_a = optionDiv
          .append('div')
          .append("a")
          .attr("href", dNode.hypertext)
          .attr("target", "_blank")

          .append('i')
          .attr("id", "fo_i_hypertext_image")
          .attr("class", "ui "+ size +" external icon")
          .attr("style", "display:inline;"+"cursor:pointer;")
          .append("title").text(dNode.hypertext.substring(0, 20)+"...")
    }
  }

  if (thisGraph.config.customOptions.delete)
  {
    var fo_xhtml_delete_image = optionDiv
        .append('div')
         .append('i')
          .attr("id", "fo_i_delete_image")
          .attr("class", "ui "+ size +" delete icon")
        .attr("style", "display:inline;"+"cursor:pointer;")
        .on("click",function(){
          thisGraph.d3Data.nodes = thisGraph.deleteNodeFromD3Nodes(dNode["@id"], thisGraph.d3Data.nodes);
          thisGraph.d3Data.edges = thisGraph.deleteEdgesOfNodeFromD3Edges(dNode["@id"], thisGraph.d3Data.edges);
          thisGraph.resetStateNode();
          thisGraph.initializeDisplay();
          thisGraph.initializeSimulation();
        })
        .append("title").text("Delete the node")
  }

  if (thisGraph.config.debug) console.log("displayOptionMenu end");
}

FluidGraph.prototype.addGhostNeighbours = function(neighbour_type_box, dNode, typeGostNeighbour) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("addGhostNeighbours start");

  var labelTypeGostNeighbour = typeGostNeighbour.split(":").pop();
  var strokeNeighbourColorTypeRgba = thisGraph.customNodes.strokeNeighbourColorTypeRgba[typeGostNeighbour];
  var colorTypeRgba = thisGraph.customNodes.colorTypeRgba[typeGostNeighbour];

  var ghost_neighbour = neighbour_type_box
    .append("div")
    .attr("class", "ghost_neighbour")
    .attr("id", "ghost_neighbour_"+labelTypeGostNeighbour)
    .attr("style", "background-color:rgba(" + colorTypeRgba
        + "," + thisGraph.customNodes.strokeOpacityGhostNeighbour + ");"
        + "cursor:text;"
        // + "border:2px;"
        + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
        + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
        + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
        + "padding:2px;"
        + "display: flex;"
        + "flex-direction:row;"
        + "justify-content:space-around;"
        + "align-content:center;"
        + "align-items:center;"
        + "height:"+thisGraph.customNodes.heightGhostNeighbour+"px;"
    )

  var ghost_neighbour_i = ghost_neighbour
    .append("div")
    .attr("class", "ghost_neighbour_i")
    .attr("id", "ghost_neighbour_i_"+labelTypeGostNeighbour)
    .attr("style", function(d) {
      return "background-color:rgba(" + colorTypeRgba
                                  + "," + thisGraph.customNodes.strokeOpacityGhostNeighbour + ");"
                                  + "display:inline-block;"
                                  + "text-align: center;padding:5px;"
                                  + "width:"+thisGraph.customNodes.widthGhostNeighbourImage+"px;"
                                  + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
  })
  .append('i')
  .attr("id", "fo_i_center_image")
  .attr("class", "ui disabled large add circle icon")
   .attr("style", "display:inline;")

  var ghost_neighbour_txt = ghost_neighbour
  .append("div")
  .attr("class", "ghost_neighbour_txt")
  .attr("id", "ghost_neighbour_txt_"+labelTypeGostNeighbour)
  .attr("style", "background-color:rgba(" + thisGraph.customNodes.neighbourColorTypeRgba[typeGostNeighbour]
                                 + "," + thisGraph.customNodes.strokeOpacity + ");"
                                 + "display:inline-block;"
                                 + "text-align: left;"
                                 + "padding-left:3px;"
                                 + "overflow:auto;"
                                 + "width:"+thisGraph.customNodes.widthGhostNeighbourTxt+"px;"
                                 + "height:"+thisGraph.customNodes.heightGhostNeighbourTxt+"px;"
  )
  .text(labelTypeGostNeighbour+" ?")
  .on("click",function(d){
    var el = d3.select(this);
    el.attr("contentEditable", "true");
    el.node().focus()

    var elDiv = d3.select(this.parentNode);
    elDiv.style("height", thisGraph.customNodes.heightGhostNeighbourTxtMax+"px");
    el.style("height", thisGraph.customNodes.heightGhostNeighbourTxtMax-5+"px");
    el.style("border", "2px solid white");
  })

  var ghost_neighbour_button = ghost_neighbour
    .append("div")
    .attr("class", "ghost_neighbour_button")
    .attr("id", "ghost_neighbour_button_"+labelTypeGostNeighbour)
    .attr("style", function(d) {
      return "background-color:rgba(" + colorTypeRgba
                                  + "," + thisGraph.customNodes.strokeOpacityGhostNeighbour + ");"
                                  + "display:inline-block;"
                                  + "text-align: center;padding:5px;"
                                  + "width:"+thisGraph.customNodes.widthGhostNeighbourImage+"px;"
                                  + "-moz-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "-webkit-border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
                                  + "border-radius:" + thisGraph.customNodesText.curvesCorners + "px;"
  })
  .on("click",function(d){
    // Warning : mix of JQuery first (for the text) and then D3js syntax
    var $elTxt = $("#ghost_neighbour_txt_"+labelTypeGostNeighbour);
    var elTxtValue = $elTxt.text();
    var elType = labelTypeGostNeighbour;
    thisGraph.addNewNeighbour(d, elType, elTxtValue);

    //After creating the new neighbour
    $elTxt.text(elType+" ?");

    var el = d3.select(this);
    var elDiv = d3.select(this.parentNode);
    var elTxt = elDiv.select("#ghost_neighbour_txt_"+labelTypeGostNeighbour)
    elDiv.style("height", thisGraph.customNodes.heightGhostNeighbour+"px");
    elTxt.style("height", thisGraph.customNodes.heightGhostNeighbourTxt+"px");
    elTxt.style("border", "");
  })
  .append('i')
   .attr("id", "fo_i_center_image")
   .attr("class", "ui large play icon")
   .attr("style", "display:inline;cursor:pointer")

  if (thisGraph.config.debug) console.log("addGhostNeighbours end");
}

FluidGraph.prototype.addNewNeighbour = function(dNode, ghostNeighbourType, ghostNeighbourLabel) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("addNewNeighbour start");

  console.log("addNewNeighbour start :", thisGraph.state.openedNode);

  var d3Node = thisGraph.getD3NodeFromIdentifier(dNode["@id"]);
  var divNeighbourBoxType = d3Node.select("#neighbour_type_box_"+ghostNeighbourType)
  var nbNeighbours = divNeighbourBoxType.node().childElementCount-1;
  var divAddLinkButtonPosition = $("#ghost_neighbour_button_"+ghostNeighbourType).position();

  var xNode = thisGraph.customNodes.widthOpenedNeighbours
                  + divAddLinkButtonPosition.left
                  + (nbNeighbours*thisGraph.customNodes.widthGhostNeighbourButton);

  var yNode = divAddLinkButtonPosition.top
                  + (nbNeighbours*thisGraph.customNodes.widthGhostNeighbourButton);

  var preparedNode = {
                  "label" : ghostNeighbourLabel,
                  "@type" : thisGraph.customNodes.blankNodeType.split(":")[0]+":"+ghostNeighbourType,
                  "x" : xNode,
                  "y" : yNode,
                  "px" : xNode,
                  "py" : yNode,
                };

  thisGraph.addNeighbourInBox(divNeighbourBoxType, preparedNode, xNode, yNode)
  var newNode = thisGraph.addDataNode(thisGraph, preparedNode);

  thisGraph.drawNewNode(dNode, preparedNode);

  thisGraph.addDataLink(dNode, newNode);
  thisGraph.drawNewLink(newNode,dNode);
  thisGraph.MakeNodeLastInSvgSequence(dNode);

  if (thisGraph.config.debug) console.log("addNewNeighbour end");
}


FluidGraph.prototype.openVideoInNode = function(d3Node, dNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("openVideoInNode start");

  //Close precedent opened node
  if (thisGraph.state.openedNode)
    thisGraph.closeNode.call(thisGraph, "opened");

  if (thisGraph.state.editedNode)
    thisGraph.closeNode.call(thisGraph, "edited");

  var el = d3Node;
  var p_el = d3.select(d3Node.node().parentNode); //p_el = g#node

  el.select("#fo_content_closed_node_label").remove();
  el.select("#fo_content_closed_node_events").remove();

  var totalHeight = thisGraph.customNodes.heightVideoInNode;

  var xNodeCircle = thisGraph.customNodes.widthOpened / 2
  var yNodeCircle = totalHeight/2;
  var cyCircleIndex = totalHeight/2-5;
  var dyTextIndex = (totalHeight/2)-10;
  var cyCircleType = (totalHeight/2)-10;
  var yTypeImage = (totalHeight/2)-22;
  var ylabel = totalHeight/2;

  el
    .select("#circle_index")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("cy", -cyCircleIndex)

  el
    .select("#text_index")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("dy", -dyTextIndex)

  el
    .select("#circle_type")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("cy", cyCircleType)

  el
    .select("#fo_type_image")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("y", yTypeImage)

  el
    .select("#nodecircle")
    .transition()
    .duration(thisGraph.customNodes.transitionDurationOpen)
    .delay(thisGraph.customNodes.transitionDelay)
    .ease(thisGraph.customNodes.transitionEasing)
    .attr("x", -xNodeCircle)
    .attr("y", -yNodeCircle)
    .attr("width", thisGraph.customNodes.widthOpened)
    .attr("height", totalHeight)
    .attr("rx", thisGraph.customNodes.curvesCornersOpenedNode)
    .attr("ry", thisGraph.customNodes.curvesCornersOpenedNode)
    .on("end", function(d) {
      thisGraph.displayVideoInOpenedNode.call(thisGraph, d3.select(this), d)
    })

  thisGraph.state.openedNode = d3Node.node();

  if (thisGraph.config.debug) console.log("openVideoInNode end");
}

FluidGraph.prototype.displayVideoInOpenedNode = function(d3Node, dNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("displayVideoInOpenedNode start");

  var el = d3Node.node();
  var p_el = d3.select(d3Node.node().parentNode); //p_el = g#node

  var totalHeight = thisGraph.customNodes.heightVideoInNode;

  var xNode = -thisGraph.customNodes.widthOpened/2;
  var yNode = -(totalHeight/2)

  var fo_content_opened_node = p_el
        .append("foreignObject")
        .attr("id","fo_content_opened_node")
        .attr("x", xNode)
        .attr("y", yNode)
        .attr("width", thisGraph.customNodes.widthOpened)
        .attr("height", totalHeight)

  var fo_xhtml_content_open_node = fo_content_opened_node
        .append('xhtml:div')
        .attr("class", "fo_xhtml_content_open_node")
        .attr("id", "fo_xhtml_content_open_node")
        //Warning : using css doesn't work !?
        .attr("style","cursor:"+thisGraph.customNodes.cursor+";"
                      +"position:static;"
                      +"width:"+thisGraph.customNodes.widthOpened+"px;"
                      // +"height:"+totalHeightMoreOption+"px;"
                    )
        .on("mousedown",null)
        .on("mouseup",null)
        .on("click",null)

  /*
   *
   * Label
   *
   * */

  var fo_xhtml_content_open_node_label = fo_xhtml_content_open_node
        .append('div')
        .attr("class", "fo_xhtml_content_open_node_label")
        .attr("id", "fo_xhtml_content_open_node_label")
        //Warning : using css doesn't work !?
        .attr("style", "width:"+thisGraph.customNodes.widthOpened+"px;"
                      // +"height:"+thisGraph.customNodes.heightOpenedLabel+"px;"
                      +"cursor:"+thisGraph.customNodes.cursor+";"
                      +"position:static;")
        .text(function(d){
          if (d.label.length > thisGraph.customNodes.maxCharactersInOpenNodeLabel)
            label = d.label.substring(0,thisGraph.customNodes.maxCharactersInOpenNodeLabel)+" ...";
          else {
            label = d.label;
          }
          return label;
        })

  /*
   *
   * Vidéo
   *
   * */

  var fo_xhtml_content_open_node_video = fo_xhtml_content_open_node
        .append('div')
        .attr("class", "fo_xhtml_content_open_node_video")
        .attr("id", "fo_xhtml_content_open_node_video")
        //Warning : using css doesn't work !?
        .html(function(d){
          var html_video = "<iframe src="+d.hypertext+" width='200px' allowfullscreen>vidéo</iframe>"
          return html_video;
        })


  if (thisGraph.config.debug) console.log("displayVideoInOpenedNode end");
}
