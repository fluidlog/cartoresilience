FluidGraph.prototype.getProportionalRadius = function(d) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getProportionalRadius start");

  let radius;

  if (thisGraph.config.proportionalNodeSize)
  {
    let nbNeighbourNodes = d.neighbours.length+1;

    radius = thisGraph.customNodes.minRadius + (nbNeighbourNodes*thisGraph.customNodes.indiceProportionalRadius);
    if (radius > thisGraph.customNodes.maxRadius)
      radius = thisGraph.customNodes.maxRadius
  }
  else radius = thisGraph.customNodes.maxRadius

  if (thisGraph.config.debug) console.log("getProportionalRadius end");

  return radius
}

FluidGraph.prototype.fade = function(opacity){
  if (thisGraph.config.debug) console.log("fade start");

  // Attention : la manière de faire ci-dessous ne fonctionne pas si on affiche l'index, car ça décale tout...
  // Mais je pense que c'est plus rapide que je faire un select dans le DOM...
  return d => {
    thisGraph.svgNodes.style('stroke-opacity', function (o) {
      const thisOpacity = d.neighbours.includes(o.i) || d.i == o.i ? 1 : opacity;
      let node = this.childNodes;
      node[0].setAttribute('fill-opacity', thisOpacity);
      node[0].setAttribute('opacity', thisOpacity);

      if (node[1])
      {
        let foreignObject1 = node[1];
        let foreignObject1_div = node[1].childNodes[0];
        foreignObject1_div.setAttribute('opacity', thisOpacity); // First foreignObject > div

        let foreignObject1_div_div = foreignObject1_div.childNodes[0];
        let getStyle = foreignObject1_div_div.getAttribute("style");
        foreignObject1_div_div.setAttribute('style', getStyle+"opacity:"+thisOpacity+";");
      }

      if (node[2])
      {
        let foreignObject2 = node[2];
        let foreignObject2_div = node[2].childNodes[0]
        foreignObject2_div.setAttribute('opacity', thisOpacity);
      }

      return thisOpacity;
    });

    thisGraph.svgEdges.style('stroke-opacity', o => (o.source === d || o.target === d ? 1 : opacity));
  }
}

FluidGraph.prototype.OrderNodesInSvgSequence = function() {
  //Make node ordred by index
  thisGraph.bgElement.selectAll("#node").sort(function(a, b) { // select the parent and sort the path's
      if (a.index > b.index) return 1; // a is not the hovered element, send "a" to the back
      else return -1; // a is the hovered element, bring "a" to the front
  });
}

FluidGraph.prototype.MakeNodeFirstInSvgSequence = function(d) {
  //Make drag node on the back (first of the list)
  thisGraph.bgElement.selectAll("#node").sort(function(a, b) { // select the parent and sort the path's
      if (a.index != d.index) return 1; // a is not the hovered element, send "a" to the back
      else return -1; // a is the hovered element, bring "a" to the front
  });
}

FluidGraph.prototype.MakeNodeLastInSvgSequence = function(d) {
  //Make drag node on the front (last of the list)
  thisGraph.bgElement.selectAll("#node").sort(function(a, b) { // select the parent and sort the path's
      if (a.index == d.index) return 1; // a is not the hovered element, send "a" to the back
      else return -1; // a is the hovered element, bring "a" to the front
  });
}

FluidGraph.prototype.getNodeIndex = function() {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getNodeIndex start");
  var nextNodeIndex = 0;

  thisGraph.d3Data.nodes.forEach(function (node){
    if (node.index > nextNodeIndex)
      nextNodeIndex = node.index;
  })

  if (thisGraph.config.debug) console.log("getNodeIndex end");
  return nextNodeIndex+1;
}

FluidGraph.prototype.getIndexNodeFromIdentifier = function(d3Nodes, identifier) {
/* Possibilité à creuser...
d3Nodes.every(function(node, index){
  if (d3Nodes[i]["@id"] === identifier)
    return i;
}
*/
  for (var i = 0, len = d3Nodes.length; i < len; i++) {
    if (d3Nodes[i]["@id"] === identifier)
      return i;
  }
  return -1;
}

FluidGraph.prototype.getIdNodeFromIdentifier = function(d3Nodes, identifier) {
  for (var i = 0, len = d3Nodes.length; i < len; i++) {
    if (d3Nodes[i]["@id"] === identifier)
      return d3Nodes[i].index;
  }
  return -1;
}

FluidGraph.prototype.getCountIdNodeFromI = function(d3Nodes, i) {
  for (var countId = 0, len = d3Nodes.length; countId < len; countId++) {
    if (d3Nodes[countId].i == i)
      return countId;
  }
  return false;
}

FluidGraph.prototype.getTypedNodeFromI = function(d3Nodes, i) {
  for (var countId = 0, len = d3Nodes.length; countId < len; countId++) {
    if (d3Nodes[countId].i == i)
      return d3Nodes[countId]["@type"];
  }
  return false;
}

FluidGraph.prototype.getD3NodeFromIdentifier = function(dNodeIdentifier) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getD3NodeFromIdentifier start");

  var d3Node;
  var allNodes = d3.selectAll("#node");
  var allNodesGroups = d3.selectAll("#node")._groups;
  var allNodesList = allNodesGroups[0];
  allNodesList.forEach(function (svgNode){
    if (svgNode.__data__["@id"] == dNodeIdentifier)
    {
      d3Node = d3.select(svgNode);
    }
  })

  if (thisGraph.config.debug) console.log("getD3NodeFromIdentifier end");

  return d3Node;
}

FluidGraph.prototype.getD3NodeFromLabel = function(label) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getD3NodeFromIdentifier start");

  var d3Node;
  var allNodes = d3.selectAll("#node");
  var allNodesGroups = d3.selectAll("#node")._groups;
  var allNodesList = allNodesGroups[0];
  allNodesList.forEach(function (svgNode){
    if (svgNode.__data__.label === label)
    {
      d3Node = d3.select(svgNode);
    }
  })

  if (thisGraph.config.debug) console.log("getD3NodeFromIdentifier end");

  return d3Node;
}

FluidGraph.prototype.getNeighbourNodesAndLinks = function(rootNode) {
  thisGraph = this;

  if (thisGraph.config.debug) console.log("getNeighbourNodesAndLinks start");

  var dataToUse;
  if (thisGraph.d3DataFiltered.nodes.length > 0)
    dataToUse = thisGraph.d3DataFiltered // External Filtered (Semapps)
  else dataToUse = thisGraph.d3Data; // Local Fludy

  var edgeType;
  var targetNode;
  var targetNodeCountId;
  var neighbours = {};
  neighbours.nodes = [];
  neighbours.edges = [];

  //First, the selected node
  neighbours.nodes.push(rootNode);

  rootNode.neighbours.forEach(function(neighbourIndex){
    // On vérifie que les neighbourIndex sont bien dans les datas (en cas de filtre)
    targetNodeCountId = thisGraph.getCountIdNodeFromI(dataToUse.nodes,neighbourIndex);
    if (targetNodeCountId)
    {
      //Nodes
      targetNode = dataToUse.nodes[targetNodeCountId];
      neighbours.nodes.push(targetNode);

      //links
      edgeType = thisGraph.linkedByIndex[neighbourIndex+","+rootNode.i] || thisGraph.linkedByIndex[rootNode.i+","+neighbourIndex];
      neighbours.edges.push({
        "@type" : edgeType,
        source: rootNode,
        target: targetNode
      });
    }
  })

  if (thisGraph.config.debug) console.log("getNeighbourNodesAndLinks end");

  return neighbours;
}
